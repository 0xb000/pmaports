# Reference: <https://postmarketos.org/devicepkg>
pkgname=device-motorola-osprey
pkgver=2
pkgrel=0
pkgdesc="Motorola Moto G 3rd gen. (2015)"
url="https://postmarketos.org"
arch="aarch64"
license="MIT"
depends="postmarketos-base mkbootimg soc-qcom-msm8916"
makedepends="devicepkg-dev"
source="deviceinfo"
options="!check !archcheck"

subpackages="
	$pkgname-kernel-mainline:kernel_mainline
	$pkgname-kernel-mainline-modem:kernel_mainline_modem
	$pkgname-nonfree-firmware:nonfree_firmware
	$pkgname-nonfree-firmware-modem:nonfree_firmware_modem
"

build() {
	devicepkg_build $startdir $pkgname
}

package() {
	devicepkg_package $startdir $pkgname
}

kernel_mainline() {
	pkgdesc="Close to mainline kernel (no modem, audio routed directly)"
	depends="linux-postmarketos-qcom-msm8916"
	devicepkg_subpackage_kernel $startdir $pkgname $subpkgname
}

kernel_mainline_modem() {
	pkgdesc="Close to mainline kernel (non-free modem, audio routed through ADSP)"
	depends="linux-postmarketos-qcom-msm8916 soc-qcom-msm8916-modem"
	devicepkg_subpackage_kernel $startdir $pkgname $subpkgname
}

nonfree_firmware() {
	pkgdesc="GPU/Wi-Fi/Bluetooth/Video(/Modem) firmware"
	depends="linux-firmware-qcom firmware-motorola-osprey-wcnss firmware-motorola-osprey-venus"
	mkdir "$subpkgdir"
}

nonfree_firmware_modem() {
	pkgdesc="Modem firmware"
	depends="firmware-motorola-osprey-modem"
	install_if="$pkgname-nonfree-firmware $pkgname-kernel-mainline-modem"
	mkdir "$subpkgdir"
}

sha512sums="84722de140aee0e6b11b49c1cd5df40efcc68f64637d646623f30b9aacfc45aab6707719c5bed3b117660f1018d7dd69551b8a5a7704f6246b944dca28d42f9c  deviceinfo"
