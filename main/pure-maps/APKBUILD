# Contributor: Bart Ribbers <bribbers@disroot.org>
# Contributor: Newbyte <newbytee@protonmail.com>
# Maintainer: Newbyte <newbytee@protonmail.com>
pkgname=pure-maps
pkgver=2.4.1
pkgrel=1
_commit_geomag="8eb9a730c8643fb7d63fdee4fd9a195ee8ba4df2"
pkgdesc="Maps and navigation"
url="https://github.com/rinigus/pure-maps"
# armhf blocked by mapbox-gl-qml -> qt5-qtdeclarative-dev
arch="all !armhf"
license="GPL-3.0-or-later"
depends="
	kirigami2
	mapbox-gl-qml
	nemo-qml-plugin-dbus
	py3-gpxpy
	py3-pyotherside
	qml-module-clipboard
	qmlrunner
	qt5-qtbase-sqlite
	qt5-qtlocation
	qt5-qtmultimedia
	qt5-qtsensors
	"
makedepends="
	gettext
	py3-pyflakes
	python3
	qt5-qtbase-dev
	qt5-qtlocation-dev
	qt5-qtquickcontrols2-dev
	qt5-qttools-dev
	qtchooser
	s2geometry-dev
	"
checkdepends="py3-pytest"
subpackages="$pkgname-lang"
source="https://github.com/rinigus/pure-maps/archive/$pkgver/pure-maps-$pkgver.tar.gz
	https://github.com/rinigus/geomag/archive/$_commit_geomag/geomag-$_commit_geomag.tar.gz
	0001-replace-jsonlint-with-json.tool.patch
	0001-explicitly-invoke-python3-in-check-json.patch
	"

prepare() {
	default_prepare

	rmdir thirdparty/geomag
	mv "$srcdir/geomag-$_commit_geomag" thirdparty/geomag
}

build() {
	qmake DEFAULT_BASEMAP=OpenCycleMap DEFAULT_ROUTER=OSRM FLAVOR=kirigami PREFIX=/usr
	make
}

check() {
	# We can't use Makefile.test because we need to disable some tests
	make -f Makefile.test check
	# Disabled as they require API keys to be present
	py.test -k 'not test_geocode and not test_autocomplete_type and not test_nearby' geocoders guides poor routers
	py.test poor/test/delayed_test_config.py
}

package() {
	INSTALL_ROOT="$pkgdir" make install

	# Locales get installed to the wrong location and thus have to be moved
	# to get picked up by abuild lang()
	mv "$pkgdir"/usr/share/pure-maps/locale "$pkgdir"/usr/share
}

sha512sums="4a7a3bbb9b573abdd7b6e1f837eafcce3a732c1b330de4df813f197d8e3a3ebf33d47b81fd75c9d8f49041b5a3fbf27efd773111f38e4465b8d4930cfe90ec93  pure-maps-2.4.1.tar.gz
13e11b6cb35162315deb86c6c6240a3555760397d7aa88ac9c3348d476e9e9547b03210134119c60790511489e3f2a13afb93a3c77d40b1258c664b6fcc0425c  geomag-8eb9a730c8643fb7d63fdee4fd9a195ee8ba4df2.tar.gz
01344056bc0cfb23205bd75022177cd6cc680055cab5ee64895c894264b539de6697b97bec11fc6de18baf7b048930d16f422527635b2dcd35382a9228b8f397  0001-replace-jsonlint-with-json.tool.patch
39ddddb31b37c74fcac9b7f3263a2d04893673d21eb8fecb353d8a3047cb314cc50f973089ae6472afed2714800ed9b98c16c92ae78fc908ff6a92fb9bfa82f1  0001-explicitly-invoke-python3-in-check-json.patch"
